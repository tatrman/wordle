#!/usr/bin/env python3

from configparser import ConfigParser

# 5lwords is hardcoded list of 5l words
# config file is wordle.conf
# letters in 
# letters out

def show():
    for x in filtered:
        print(x.strip())

DEBUG=False

config = ConfigParser()
config.read('wordle.conf')

if DEBUG:
    print("Reading config wordle.conf")

words_in = config.get('letters','in' )
words_out = config.get('letters','out')




  
allwords = []
if DEBUG:
    print("Opening 5lwords")
# what about embedding words here
f = open("./5lwords")
for x in f:
    allwords.append(x)

if DEBUG:
    print(f"Words at the beginning: { len(allwords) } ")

filtered = allwords

for x in words_out:
    if DEBUG:
        print(f"Filtering out letter {x}")
    tmp_filtered = filter(lambda word: x not in word, filtered)
    filtered = list(tmp_filtered)

# check if there are any found lettters
if len(words_in) > 0:

    for x in words_in:
        if DEBUG:
            print(f"Filtering in letter {x}")
        tmp_filtered = filter(lambda word: x in word, filtered)
        filtered = list(tmp_filtered)

dlzka = len(filtered)

if DEBUG:
    print(f"Length after filtering: {dlzka}")

# now check position 
# loop over letters we know are matched

for x in words_in:
    try: 
        pos = config.get('position',x)
        # print(f"Position {pos} for {x}")
        if pos.startswith('!'):
            # print(f"Letter {x} Not {pos}")
            pos = pos[1:]
            for a in pos:
                print(f"Looping over {pos} as {a}")
                if a =="1":
                    # print(f"Looking for words NOT starting with {x}")
                    tmp_filtered = filter(lambda word: not word.startswith(x),filtered )
                    filtered = list(tmp_filtered)
                elif a == "5":
                    print(f"Looking for words NOT ending with {x}")
                    tmp_filtered = filter(lambda word: not word.strip().endswith(x),filtered )
                    filtered = list(tmp_filtered)
                else:
                    #print(f"Looking for words where {x} is NOT in position {a}")
                    tmp_filtered = [word for word in filtered if word[int(a)-1] != x ]
                    filtered = list(tmp_filtered)

        else:
            for a in pos:
                if a == "1":
                    #print(f"Looking for words starting with {x}")
                    tmp_filtered = filter(lambda word: word.startswith(x),filtered )
                    filtered = list(tmp_filtered)
                elif a == "5":
                    #print(f"Looking for words ending with {x}")
                    tmp_filtered = filter(lambda word: word.strip().endswith(x),filtered )
                    filtered = list(tmp_filtered)
                else:
                    #print(f"Looking for words where {x} is in position {a}")
                    tmp_filtered = filter(lambda word: word[int(a)-1] == str(x) ,filtered )
                    filtered = list(tmp_filtered)

    except Exception as e:
        print(f"Exception {e}")
    
show()
