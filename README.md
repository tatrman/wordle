# wordle game solver


1st find 5 letter words

    grep '^[a-z][a-z][a-z][a-z][a-z]$' /usr/share/dict/words > ~/tmp/5lwords

* 5lwords ... list of 5l words
* wordle.py ... program
* wordle.conf ... config

- section *position* should look like this

```
[position]
a=3
b=!34
```

Meaning:
- *a* in position 3
- *b* NOT in position 3 and 4

Initially I was using  `egrep '..a..'` or `egrep -v '.s...'` with pipes, but it's inconvenient.


## config

*letters* section have *in* and *out* letters
- in ... found letters
- out ... exclude these letters
```
[letters]
in=eri
out=sauctbfl

[position]
e=2
r=!134
```

## usage

   ./wordle.py 


Dont' forget to manually edit `wordle.conf` and add found words to `in` and excluded to `out`.
If you know about position, add to section *position*.
`wordle.conf` is expected next to `wordle.py` for now.


