# Changes
## v0.2.0 2022-01-26 

Working positional filtering
Removed `-y`

## v0.1.0 2022-01-19 

Working, *in*, *out* in letters done
`-y` cmd line parameter not to ask
